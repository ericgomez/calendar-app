import moment from 'moment'
import { types } from '../types/types'

const initialState = {
  events: [
    {
      id: new Date().getTime(),
      title: 'Birthday of the boss',
      start: moment().toDate(),
      end: moment()
        .add(2, 'hours')
        .toDate(), // add 2 hours
      bgcolor: '#fafafa',
      notes: 'This is a note about the event.',
      user: {
        _id: '5d2b2b7d6b5e1359f8f9f6b0',
        name: 'Shen Zhi'
      }
    }
  ],
  activeEvent: null
}

export const calendarReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.eventSetActive:
      return {
        ...state,
        activeEvent: action.payload
      }

    case types.eventAddNew:
      return {
        ...state,
        events: [...state.events, action.payload]
      }

    case types.eventClearActiveEvent:
      return {
        ...state,
        activeEvent: null
      }

    case types.eventUpdated:
      return {
        ...state,
        // search for the event with the same id as the payload
        events: state.events.map(event =>
          // if the event id is the same as the payload id
          event.id === action.payload.id ? action.payload : event
        )
      }

    case types.eventDeleted:
      return {
        ...state,
        // filter out the event with the same id as the payload
        events: state.events.filter(
          event =>
            // if the event id is not the same as the payload id
            event.id !== state.activeEvent.id
        ),
        // clear the active event
        activeEvent: null
      }

    default:
      return state
  }
}
