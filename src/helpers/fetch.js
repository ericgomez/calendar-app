const fetchWithoutToken = (endpoint, data, method = 'GET') => {
  const url = `${process.env.REACT_APP_API_URL}${endpoint}`

  if (method === 'GET') {
    return fetch(url)
  } else {
    return fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
  }
}

const fetchWithToken = (endpoint, data, method = 'GET') => {
  const url = `${process.env.REACT_APP_API_URL}${endpoint}`

  if (method === 'GET') {
    return fetch(url, {
      headers: {
        'x-token': localStorage.getItem('token') || ''
      }
    })
  } else {
    return fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token') || ''
      },
      body: JSON.stringify(data)
    })
  }
}

export { fetchWithoutToken, fetchWithToken }
