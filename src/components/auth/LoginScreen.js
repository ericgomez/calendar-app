import React from 'react'
import { useDispatch } from 'react-redux'
import Swal from 'sweetalert2'
import { startLogin, startRegister } from '../../actions/auth'
import { useForm } from '../../hooks/useForm'
import './login.css'

export const LoginScreen = () => {
  const dispatch = useDispatch()

  // values from form login
  const [formLoginValues, handleLoginInputChange] = useForm({
    lEmail: 'eric.gomez@gmail.com',
    lPassword: '123456'
  })

  const { lEmail, lPassword } = formLoginValues

  // values from form register
  const [formRegisterValues, handleRegisterInputChange] = useForm({
    rName: 'Juana Reyes',
    rEmail: 'juana.reyes@gmail.com',
    rPassword1: '123456',
    rPassword2: '123456'
  })

  const { rName, rEmail, rPassword1, rPassword2 } = formRegisterValues

  const handleLogin = e => {
    e.preventDefault()
    dispatch(startLogin(lEmail, lPassword))
  }

  const handleRegister = e => {
    e.preventDefault()
    // console.log(formRegisterValues)

    if (rPassword1 !== rPassword2) {
      return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Passwords are not the same'
      })
    }

    dispatch(startRegister(rName, rEmail, rPassword1))
  }

  return (
    <div className='container login-container'>
      <div className='row'>
        <div className='col-md-6 login-form-1'>
          <h3>Login</h3>
          <form onSubmit={handleLogin}>
            <div className='form-group'>
              <input
                type='text'
                className='form-control'
                placeholder='Email'
                name='lEmail'
                value={lEmail}
                onChange={handleLoginInputChange}
              />
            </div>
            <div className='form-group'>
              <input
                type='password'
                className='form-control'
                placeholder='Password'
                name='lPassword'
                value={lPassword}
                onChange={handleLoginInputChange}
              />
            </div>
            <div className='form-group'>
              <input type='submit' className='btnSubmit' value='Login' />
            </div>
          </form>
        </div>

        <div className='col-md-6 login-form-2'>
          <h3>Register</h3>
          <form onSubmit={handleRegister}>
            <div className='form-group'>
              <input
                type='text'
                className='form-control'
                placeholder='Name'
                name='rName'
                value={rName}
                onChange={handleRegisterInputChange}
              />
            </div>
            <div className='form-group'>
              <input
                type='email'
                className='form-control'
                placeholder='Email'
                name='rEmail'
                value={rEmail}
                onChange={handleRegisterInputChange}
              />
            </div>
            <div className='form-group'>
              <input
                type='password'
                className='form-control'
                placeholder='Password'
                name='rPassword1'
                value={rPassword1}
                onChange={handleRegisterInputChange}
              />
            </div>

            <div className='form-group'>
              <input
                type='password'
                className='form-control'
                placeholder='Repeat Password'
                name='rPassword2'
                value={rPassword2}
                onChange={handleRegisterInputChange}
              />
            </div>

            <div className='form-group'>
              <input type='submit' className='btnSubmit' value='Create count' />
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}
