import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Modal from 'react-modal'
import DateTimePicker from 'react-datetime-picker'
import moment from 'moment'
import Swal from 'sweetalert2'
import { uiCloseModal } from '../../actions/ui'
import {
  eventAddNew,
  eventClearActiveEvent,
  eventUpdated
} from '../../actions/events'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

// Make sure to bind modal to your appElement (https://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root')

const now = moment()
  .minutes(0)
  .seconds(0)
  .add(1, 'hour')

// clone the date so that the date is not mutated
const later = now.clone().add(1, 'hours')

const initEvent = {
  title: '',
  notes: '',
  start: now.toDate(),
  end: later.toDate()
}

export const CalendarModal = () => {
  const { modalOpen } = useSelector(state => state.ui)
  const { activeEvent } = useSelector(state => state.calendar)

  const dispatch = useDispatch()

  const [dateStart, setDateStart] = useState(now.toDate())
  const [dateEnd, setDateEnd] = useState(later.toDate())
  const [titleValid, setTitleValid] = useState(true)

  const [formValues, setFormValues] = useState(initEvent)

  const { notes, title, start, end } = formValues

  useEffect(() => {
    if (activeEvent) {
      setFormValues(activeEvent)
    } else {
      // clear form
      setFormValues(initEvent)
    }
  }, [activeEvent])

  const handleInputChange = ({ target }) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value
    })
  }

  const handleStartDateChange = e => {
    setDateStart(e)
    setFormValues({ ...formValues, start: e })
  }

  const handleEndDateChange = e => {
    setDateEnd(e)
    setFormValues({ ...formValues, end: e })
  }

  const handleSubmitForm = e => {
    e.preventDefault()

    const momentStart = moment(start)
    const momentEnd = moment(end)

    if (momentStart.isSameOrAfter(momentEnd)) {
      console.log('start date is after end date')
      Swal.fire({
        title: 'Error',
        text: 'Start date is after end date',
        icon: 'error'
      })
      return
    }

    if (title.trim().length < 2) {
      setTitleValid(false)
      return
    }

    // if activeEvent is different of null we are updating an existing event
    if (activeEvent) {
      dispatch(eventUpdated(formValues))
    } else {
      // if activeEvent is null, then we are adding a new event
      dispatch(
        eventAddNew({
          ...formValues,
          id: new Date().getTime(),
          user: {
            _id: '5d2b2b7d6b5e1359f8f9f6b0',
            name: 'Shen Zhi'
          }
        })
      )
    }

    // TODO: add connection to database

    // change style of input if is valid
    setTitleValid(true)
    // close the modal
    closeModal()
  }

  const closeModal = () => {
    // TODO: close modal
    // console.log('close modal')
    dispatch(uiCloseModal())
    dispatch(eventClearActiveEvent())
    setFormValues(initEvent)
  }

  return (
    <Modal
      isOpen={modalOpen}
      // onAfterOpen={afterOpenModal}
      onRequestClose={closeModal}
      style={customStyles}
      closeTimeoutMS={200}
      className='modal'
      overlayClassName='modal-overlay'
    >
      <h1> {activeEvent ? 'Update Event' : 'New Event'} </h1>
      <hr />
      <form className='container' onSubmit={handleSubmitForm}>
        <div className='form-group'>
          <label>Date and time start</label>
          <DateTimePicker
            onChange={handleStartDateChange}
            value={dateStart}
            format={'dd-MM-yyyy hh:mm a'}
            className='form-control'
          />
        </div>

        <div className='form-group'>
          <label>Date and time end</label>
          <DateTimePicker
            onChange={handleEndDateChange}
            value={dateEnd}
            minDate={dateStart}
            format={'dd-MM-yyyy hh:mm a'}
            className='form-control'
          />
        </div>

        <hr />
        <div className='form-group'>
          <label>Title and notes</label>
          <input
            type='text'
            className={`form-control ${!titleValid && 'is-invalid'}`}
            placeholder='Event title'
            name='title'
            autoComplete='off'
            value={title}
            onChange={handleInputChange}
          />
          <small id='emailHelp' className='form-text text-muted'>
            Description short
          </small>
        </div>

        <div className='form-group'>
          <textarea
            type='text'
            className='form-control'
            placeholder='Notes'
            rows='5'
            name='notes'
            value={notes}
            onChange={handleInputChange}
          ></textarea>
          <small id='emailHelp' className='form-text text-muted'>
            Additional information
          </small>
        </div>

        <button type='submit' className='btn btn-outline-primary btn-block'>
          <i className='far fa-save'></i>
          <span> Save</span>
        </button>
      </form>
    </Modal>
  )
}
