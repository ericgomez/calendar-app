export const types = {
  uiOpenModal: '[ui] Open Modal',
  uiCloseModal: '[ui] Close Modal',

  eventSetActive: '[event] Set Active',
  eventAddNew: '[event] Add New',
  eventClearActiveEvent: '[event] Clear Active Event',
  eventUpdated: '[event] Updated',
  eventDeleted: '[event] Deleted',

  authCheckingFinished: '[auth] Checking Finished',
  authStartLogin: '[auth] Start Login',
  authLogin: '[auth] Login',
  authStartRegister: '[auth] Start Register',
  authStartTokenRenew: '[auth] Start Token Renew',
  authLogout: '[auth] Logout'
}
