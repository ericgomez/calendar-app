import React, { useEffect } from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { useDispatch } from 'react-redux'

import { LoginScreen } from '../components/auth/LoginScreen'
import { CalendarScreen } from '../components/calendar/CalendarScreen'
import { startChecking } from '../actions/auth'
import { useSelector } from 'react-redux'
import { PublicRoute } from '../routers/PublicRoute'
import { PrivateRoute } from '../routers/PrivateRoute'

export const AppRouter = () => {
  const dispatch = useDispatch()

  const { checking, uid } = useSelector(state => state.auth)

  useEffect(() => {
    dispatch(startChecking())
  }, [dispatch])

  if (checking) {
    return <div>Checking...</div>
  }

  return (
    <Router>
      <Routes>
        <Route
          path='/login'
          element={
            <PublicRoute isAuthenticated={!!uid} element={<LoginScreen />} />
          }
        />

        <Route
          path='/'
          element={
            <PrivateRoute
              isAuthenticated={!!uid}
              element={<CalendarScreen />}
            />
          }
        />

        <Route path='*' element={<CalendarScreen />} />
      </Routes>
    </Router>
  )
}
