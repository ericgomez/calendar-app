import { Navigate } from 'react-router-dom'
import PropTypes from 'prop-types'

export const PublicRoute = ({ isAuthenticated, element }) => {
  return isAuthenticated ? <Navigate to='/' replace /> : element
}

// PropTypes
PublicRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  element: PropTypes.object.isRequired
}
