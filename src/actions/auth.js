import Swal from 'sweetalert2'
import { fetchWithoutToken, fetchWithToken } from '../helpers/fetch'
import { types } from '../types/types'

// As we need to perform asynchronous actions, we need to use redux-thunk

export const startLogin = (email, password) => {
  // Implementing dispatch of redux-thunk
  return async dispatch => {
    const resp = await fetchWithoutToken('/auth', { email, password }, 'POST')
    const body = await resp.json()

    // Save token to localStorage
    if (body.ok) {
      localStorage.setItem('token', body.token)
      localStorage.setItem('tokenInitDate', new Date().getTime())

      dispatch(login({ uid: body.uid, name: body.name }))
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: body.msg
      })
    }
  }
}

export const startRegister = (name, email, password) => {
  // Implementing dispatch of redux-thunk
  return async dispatch => {
    const resp = await fetchWithoutToken(
      '/auth/new',
      { name, email, password },
      'POST'
    )
    const body = await resp.json()

    // Save token to localStorage
    if (body.ok) {
      localStorage.setItem('token', body.token)
      localStorage.setItem('tokenInitDate', new Date().getTime())

      dispatch(login({ uid: body.uid, name: body.name }))
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: body.msg
      })
    }
  }
}

export const startChecking = () => {
  // Implementing dispatch of redux-thunk
  return async dispatch => {
    const resp = await fetchWithToken('/auth/renew')
    const body = await resp.json()

    // Save token to localStorage
    if (body.ok) {
      localStorage.setItem('token', body.token)
      localStorage.setItem('tokenInitDate', new Date().getTime())

      dispatch(login({ uid: body.uid, name: body.name }))
    } else {
      dispatch(checkingFinish())
    }
  }
}

const checkingFinish = () => ({ type: types.authCheckingFinished })

const login = user => ({ type: types.authLogin, payload: user })

export const startLogout = () => {
  return dispatch => {
    localStorage.removeItem('token')
    dispatch(logout())
  }
}

const logout = () => ({ type: types.authLogout })
